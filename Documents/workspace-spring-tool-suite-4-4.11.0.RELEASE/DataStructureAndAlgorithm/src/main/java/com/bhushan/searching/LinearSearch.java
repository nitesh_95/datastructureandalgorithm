package com.bhushan.searching;

public class LinearSearch {
	public static boolean linearSearch(int[] arr, int key) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == key)
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		LinearSearch linearSearchs = new LinearSearch();
		int[] arr = { 1, 2, 4, 5, -2, 6 };
		int key = 12;
		boolean linearSearch = linearSearchs.linearSearch(arr, key);
		System.out.println(linearSearch);
	}
}
