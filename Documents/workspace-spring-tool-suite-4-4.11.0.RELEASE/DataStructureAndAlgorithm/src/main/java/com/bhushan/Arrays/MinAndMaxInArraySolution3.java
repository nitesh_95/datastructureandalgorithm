package com.bhushan.Arrays;

//Initialisation should be something which is in the array ..
//if we initialize with 0 it fails when the array elements are negative..
public class MinAndMaxInArraySolution3 {
	public static void main(String[] args) {
		int ar[] = { 1, 2, 3, 4, 5, 6 };
		int max = ar[0];
		for (int i = 0; i < ar.length; i++) {
			if (ar[i] > max) {
				max = ar[i];
			}
		}
		System.out.println(max);
	}
}
