package com.bhushan.Arrays;

import java.util.Scanner;

public class Factorial {

	 public static void main(String[] args) {
		 long factorial =1;
	       Scanner sc = new Scanner(System.in);
	        long num = sc.nextInt();
	        for (long i = 1; i <=num; i++) {
				factorial = factorial * i;
			}
	        System.out.println(factorial);
	    }
	}
