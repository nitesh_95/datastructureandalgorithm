package com.bhushan.ds;

import java.util.Scanner;

public class ArmstrongNumber {

	public static boolean findArmStrongNumber(String num) {
		int sum = 0;
		for (int i = 0; i < num.length(); i++) {
			char charAt = num.charAt(i);
			sum = sum + (charAt-48) * (charAt-48) * (charAt-48);
			System.out.println(sum);
		}
		int parseInt = Integer.parseInt(num);
		if (parseInt == sum) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the element which u want to search");
		String nextInt = scanner.nextLine();
		boolean armStrongNumber = findArmStrongNumber(nextInt);
		System.out.println(armStrongNumber);
	}

}
