package com.bhushan.Arrays;
// initalisation should be something which must be in array 
public class MinAndMaxInArraySolution2 {
	public static void main(String[] args) {
		int ar[] = { 1, 2, 3, 4, 5, 6 };
		int n = ar.length;
		int min = ar[0];
		for (int i = 0; i < ar.length; i++) {
			if (ar[i] < min) {
				min = ar[i];
			}
		}
		System.out.println(min);
	}

}
