package com.bhushan.ds;

import java.util.Scanner;

public class PatternPrinting {

//*
//* *
//* * *
//* * * *
//* * * * *

	public static void firstPattern(int num) {
		for (int row = 1; row <= num; row++) {
			for (int column = 1; column <=row; column++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int nextInt = scanner.nextInt();
		firstPattern(nextInt);
	}
}
