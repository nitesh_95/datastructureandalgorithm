package com.bhushan.Arrays;

import java.util.Scanner;
//Optimized solution for adding all digit in the sum
public class SumOfAllDigitSolution2 {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int sum = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		String num = scanner.nextLine();
		for (int i = 0; i < num.length(); i++) {
			char charAt = num.charAt(i); // when we find the chatAt it returns the ascii code for the same , so after getting the ascii code we need
			//to convert to some number , Generally ascii start with 48 , 
			sum = sum + (charAt-48);
		}
		System.out.println(sum);
	}

}
