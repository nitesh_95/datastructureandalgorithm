package com.bhushan.ds;

import java.util.Scanner;

public class Pattern2 {
	
//	   *
//	  **
//	***
//*	***

	public static void secondPattern(int num) {
		for (int row = 1; row <= num; row++) {
			for (int column = 1; column <=num- row; column++) {
				System.out.print(" ");
			}
			for (int ch = 1; ch <= row; ch++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}


//	****
//	***
//	**
//	*

//	public static void firstPattern(int num) {
//		for (int row = 1; row <= num; row++) {
//			for (int column = 1; column <= num - row + 1; column++) {
//				System.out.print("*");
//			}
//			System.out.println();
//		}
//	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int nextInt = scanner.nextInt();
//		firstPattern(nextInt);
		secondPattern(nextInt);
	}
}
