package com.bhushan.ds;

public class DuplicateInArray {

	public static int duplicateNumber() {
		int num = 0;
		int[] arr = { 6, 1, 4, 3, -5, -1, 3 };
		boolean flag = false;

		for (int i = 0; i < arr.length; i++) {
			for (int j = i; j < arr.length; j++) {
				if (arr[i] == arr[j]) {
					flag = true;
					num = arr[i];
					break;
				} else {
					flag = false;
				}
			}
		}
		return num;
	}

	public static void main(String[] args) {
		int duplicateNumber = duplicateNumber();
		System.out.println("DuplicateNumber is " + duplicateNumber);
	}
}