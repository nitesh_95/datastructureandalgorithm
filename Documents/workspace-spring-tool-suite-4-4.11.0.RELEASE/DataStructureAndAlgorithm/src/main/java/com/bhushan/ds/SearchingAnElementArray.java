package com.bhushan.ds;

import java.util.Scanner;

public class SearchingAnElementArray {

	public static boolean searchInArray(int num) {
		int[] arr = { 6, 1, 4, 3, -5, -1 };
		boolean flag = false;
		for (int i = 0; i < arr.length; i++) {
			if (num == arr[i]) {
				flag = true;
				break;
			} else {
				flag = false;
			}

		}
		return flag;
	}

	public static int searchInArrayIndex(int num) {
		int[] arr = { 6, 1, 4, 3, -5, -1 };
		int index = -1;
		boolean flag = false;
		for (int i = 0; i < arr.length; i++) {
			if (num == arr[i]) {
				flag = true;
				index = i;
				break;
			} else {
				flag = false;
			}

		}
		return index;

	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Number");
		int num = scanner.nextInt();
		boolean searchInArray = searchInArray(num);
		System.out.println("Element Found" + " ->" + searchInArray);
		int searchInArrayIndex = searchInArrayIndex(num);
		if (searchInArrayIndex>=0) {
			System.out.println("At Index" + searchInArrayIndex);
		}
	}

}
