package com.bhushan.ds;

import java.util.Scanner;

public class LinearSearch {

	public static boolean linearSearch(int num) {

		int[] arr = { 6, 1, 4, 3, -5, -1 };
		int count = 0;
		boolean flag = false;
		for (int i = 0; i < arr.length; i++) {
			count++;
			if (num == arr[i]) {
				flag = true;
				System.out.println(count);
				break;
			} else {
				flag = false;
			}
		}
		return flag;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int nextInt = scanner.nextInt();
		System.out.println(linearSearch(nextInt));
	}
}
