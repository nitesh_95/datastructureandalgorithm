package com.bhushan.Arrays;

import java.util.Scanner;

public class SumOfDigit {

	public static void main(String[] args) {
		int sum = 0;
		Scanner sc = new Scanner(System.in);
		String word = sc.nextLine();
		for (int i = 0; i < word.length(); i++) {
			char ch = word.charAt(i);
			sum = sum + ch-48;
		}
		System.out.println(sum);
	}
}