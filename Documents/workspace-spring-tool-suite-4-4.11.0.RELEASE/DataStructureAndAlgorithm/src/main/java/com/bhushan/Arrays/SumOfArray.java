package com.bhushan.Arrays;

//sumOfArray
public class SumOfArray {
	public static void main(String[] args) {

		int sum = 0;

		int[] num = { 1, 2, 3, 4, 5 };
		for (int i = 0; i < num.length; i++) {
			sum = num[i]+sum;
		}
		System.out.println(sum);
	}
}