package com.bhushan.ds;

import java.util.Scanner;

public class LinearSearchh {

	public static boolean linearSearch(int num) {

		int[] arr = { 6, 1, 4, 3, -5, -1 };

		boolean flag = false;
		for (int i = 0; i < arr.length; i++) {
			if (num == arr[i]) {
				flag = true;
				break;
			} else {
				flag = false;
			}
		}
		return flag;
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int nextInt = scanner.nextInt();
		System.out.println(linearSearch(nextInt));
	}
}