package com.bhushan.ds;

import java.util.Scanner;

public class BinarySearchAlgo {

	// It can be used only for sorted array only ascending order is accepted  ..

	public static boolean binarySearch(int num) {
		int[] arr = { 2, 4, 6, 10, 14, 22 };
		int lowest_index = 0;
		int highest_index = arr.length - 1;
		int count = 0;

		while (lowest_index <= highest_index) {
			count ++;
			int mid = (lowest_index + highest_index) / 2;
			if (arr[mid] > num) {
				highest_index = mid - 1;
			} else if (arr[mid] < num) {
				lowest_index = mid + 1;
			} else {
				return true;
			}
		}
		System.out.println(count);
		return false;
	}

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the element which u want to search");
		int nextInt = scanner.nextInt();
		boolean binarySearch = binarySearch(nextInt);
		System.out.println(binarySearch);
	}
}
