package com.bhushan.ds;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the element which u want to search");
		int nextInt = scanner.nextInt();
		boolean primeNumber = primeNumber(nextInt);
		System.out.println(primeNumber);
	}

	private static boolean primeNumber(int num) {
		boolean flag = false;
		for (int i = 2; i <= num / 2; i++) {
			if (num % i == 0) {
				flag = true;
				break;
			}
		}
		if (!flag) {
			return true;
		} else {
			return false;
		}
	}
}
