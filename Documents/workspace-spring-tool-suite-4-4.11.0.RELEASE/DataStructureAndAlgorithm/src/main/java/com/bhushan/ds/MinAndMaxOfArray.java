package com.bhushan.ds;

public class MinAndMaxOfArray {
	public static int maxValue() {
		int[] arr = {0, -2, -19, 8, 15, -6, 4 };
		int temp = arr[0];

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > temp) {
				temp = arr[i];
			}
		}
		return temp;
	}

	// when intializing with zero the following case fails because temp value is
	// always zero
	public static int minValue() {
		int temp = 0;
		int[] arr = { 6, 1, 4, 3 };

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < temp) {
				temp = arr[i];
			}
		}
		return temp;
	}

	public static int minValues() {
		int[] arr = { 6, 1, 4, 3, -5, -1 };
		int temp = arr[0];

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < temp) {
				temp = arr[i];
			}
		}
		return temp;
	}

	public static void main(String[] args) {
		int maxValue = maxValue();
		System.out.println("maxValue" + maxValue);
		int minValue = minValue();
		System.out.println("minValue" + minValue);
		int minValues = minValues();
		System.out.println("minValue after initializing with arr[0]" + " -> " + minValues);
	}
}
