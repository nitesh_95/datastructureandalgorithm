package com.bhushan.ds;

import java.util.Scanner;

public class FactorialProgram {

	public static int factorial(int num) {
		int factorial = 1;
		for (int i = 1; i <=num; i++) {
			factorial  = i*factorial;
		}
		return factorial;
		
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Number");
		int num = scanner.nextInt();
		int factorial = factorial(num);
		System.out.println(factorial);
	}
}
