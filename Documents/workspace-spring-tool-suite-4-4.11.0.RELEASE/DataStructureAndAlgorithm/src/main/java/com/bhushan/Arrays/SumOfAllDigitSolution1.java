package com.bhushan.Arrays;

import java.util.Scanner;


//Problem with this is if int contains very big number int cant hold it even we keep it long after certain limit
//long can also not hold such element , for OPtimized solution refer to SumOfAllDigitSolution2
public class SumOfAllDigitSolution1 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		int sum = 0;

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Number");
		int n = scanner.nextInt();

		while (n != 0) {
			int d = n % 10; //modulus refers to the remainder
			sum = sum + d;
			n = n / 10; //move to the second number
		}
		System.out.println(sum);

	}

}
