package com.bhushan.searching;

//In case of sorted array
public class LinearSearchPart2 {
	public static void main(String[] args) {
		int[] arr = { -7, -1, 1, 2, 3, 8, 11, 15 };
		int key = 11;
		boolean linearSearch = linearSearchPart2(arr, key);
		System.out.println(linearSearch);
	}

	private static boolean linearSearchPart2(int[] arr, int key) {
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == key)
				return true;
			if (arr[i] > key)
				return false;
		}
		return false;
	}

}
