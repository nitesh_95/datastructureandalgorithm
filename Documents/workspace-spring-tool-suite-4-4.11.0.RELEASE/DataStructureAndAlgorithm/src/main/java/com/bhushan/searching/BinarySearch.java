package com.bhushan.searching;

public class BinarySearch {
	public static void main(String[] args) {

		int[] arr = { -7, -1, 1, 2, 3, 8, 11, 15 };
		int key = 15;
		boolean binarySearch = binarySearch(arr, key);
		System.out.println(binarySearch);
	}

	private static boolean binarySearch(int[] arr, int key) {
		int low = 0;
		int count =0;
		int high = arr.length - 1;
		while (low <= high) {
			count++;
			int mid = (low + high) / 2;
			if (arr[mid] > key) {
				high = mid - 1;
			} else if (arr[mid] < key) {
				low = mid + 1;
			} else {
				System.out.println(count);
				return true;
			}
		}
		return false;
	}
}
