package com.bhushan.Arrays;

import java.util.Scanner;

public class MissingNumberHackerRank {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		int num[] = new int[number];
		for (int i = 0; i < num.length; i++) {
			num[i] = scanner.nextInt();
		}
		for (int j = 0; j < num.length; j++) {
			for (int i = 0; i < num.length; i++) {
				if (num[i] != num[j]) {
					System.out.println(num[i]);
				}
			}
		}
	}
}
